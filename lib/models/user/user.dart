import 'package:json_annotation/json_annotation.dart';
part 'user.g.dart';

@JsonSerializable()
class User extends Object with _$UserSerializerMixin {
  final userId;
  final id;
  final title;
  final body;
  User(this.userId, this.id, this.title, this.body);

  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);
}