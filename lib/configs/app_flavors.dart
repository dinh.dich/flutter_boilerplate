enum RunModes {
  DEV,
  PROD
}

enum AppTypes {
  DOCTOR,
  PATIENT
}

class AppFlavors {
  static RunModes RUN_MODE = RunModes.DEV;
  static AppTypes APP_TYPE = AppTypes.PATIENT;
}
