import 'package:redux/redux.dart';
import '../actions/post_actions.dart';
import '../apis/post_api.dart' as postAPI;
import '../models/post/post.dart';

void getPost(Store store, GetPost action) async {
  Post post = await postAPI.getPost(action.id);
  store.dispatch(UpdatePost(post));
}