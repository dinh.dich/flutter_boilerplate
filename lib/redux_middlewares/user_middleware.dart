import 'package:redux/redux.dart';
import '../actions/user_actions.dart';
import '../apis/user_api.dart' as userAPI;
import '../models/user/user.dart';

void getUser(Store store, GetUser action) async {
  User user = await userAPI.getUser();
  store.dispatch(UpdateUser(user));
}