import 'package:redux/redux.dart';
import '../redux/app_state_reducer.dart';
import '../actions/user_actions.dart';
import '../actions/post_actions.dart';
import 'user_middleware.dart' as userMiddleware;
import 'post_middleware.dart' as postMiddleware;

Map<Type, Function> actionMap = {
  GetUser: userMiddleware.getUser,
  GetPost: postMiddleware.getPost,
};

Middleware<AppState> apiCallMiddleware() => (Store<AppState> store, dynamic action, NextDispatcher next) {
  if (actionMap[action.runtimeType] != null) {
    Function.apply(actionMap[action.runtimeType], [store, action]);
  }
  next(action);
};