import 'package:flutter/material.dart';
import 'configs/app_flavors.dart';
import 'view/app.dart';

void main() {
  AppFlavors.RUN_MODE = RunModes.DEV;
  AppFlavors.APP_TYPE = AppTypes.PATIENT;
  return runApp(new MyApp());
}