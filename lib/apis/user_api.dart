import 'package:http/http.dart' as http;
import '../models/user/user.dart';
import 'dart:convert';
import 'dart:async';

Future<User> getUser() async {
  return http.get('https://jsonplaceholder.typicode.com/posts/1')
      .then((response) {
        return User.fromJson(json.decode(response.body));
  });
}