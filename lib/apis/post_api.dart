import 'package:http/http.dart' as http;
import '../models/post/post.dart';
import 'dart:convert';
import 'dart:async';

Future<Post> getPost(int id) async {
  return http.get('https://jsonplaceholder.typicode.com/posts/$id')
      .then((response) {
    return Post.fromJson(json.decode(response.body));
  });
}