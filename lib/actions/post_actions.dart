import '../models/post/post.dart';

class GetPost {
  int id;
  GetPost(this.id);
}

class UpdatePost {
  Post post;
  UpdatePost(this.post);
}