import '../models/user/user.dart';
import 'user_reducer.dart';
import '../models/post/post.dart';
import 'post_reducer.dart';
import 'package:redux_persist/redux_persist.dart';
import 'package:meta/meta.dart';

@immutable
class AppState {
  final User user;
  final Post post;
  AppState({this.user = null, this.post = null});

  static AppState fromJson(dynamic json) => new AppState(
    post: new Post.fromJson(json['post'])
  );

  Map toJson() => {
    'post': post.toJson()
  };
}

AppState appStateReducer(AppState state, action) {
  //for redux persist
  if (action is LoadedAction<AppState>) {
    return action.state ?? state;
  }

  return new AppState(
      user: userReducer(state.user, action),
      post: postReducer(state.post, action)
  );
}