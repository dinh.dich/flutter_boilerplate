import '../models/post/post.dart';
import '../actions/post_actions.dart';
import 'package:redux/redux.dart';

Post _updatePost(Post oldPost, UpdatePost action) => action.post;

Reducer<Post> postReducer = combineReducers<Post>([
    TypedReducer<Post, UpdatePost>(_updatePost)
]);