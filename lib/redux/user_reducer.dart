import '../models/user/user.dart';
import '../actions/user_actions.dart';
import 'package:redux/redux.dart';

User _updateUser(User oldUser, UpdateUser updateUser) => updateUser.user;

Reducer<User> userReducer = combineReducers<User>([
    TypedReducer<User, UpdateUser>(_updateUser)
]);