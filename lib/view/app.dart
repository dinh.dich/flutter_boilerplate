import 'package:flutter/material.dart';
import 'package:redux/redux.dart';
import '../redux/app_state_reducer.dart';
import '../redux_middlewares/index.dart' as apiCallMiddleware;
import '../view/home_page.dart';
import 'package:redux_persist_flutter/redux_persist_flutter.dart';
import 'package:redux_persist/redux_persist.dart';
import '../configs/app_flavors.dart';

class MyApp extends StatelessWidget {
  static final persistor = new Persistor<AppState>(
    storage: FlutterStorage('state-persist'),
    decoder: AppState.fromJson,
    debug: AppFlavors.RUN_MODE == RunModes.DEV,
  );

  final store = new Store<AppState>(appStateReducer,
    initialState: new AppState(),
    middleware: [apiCallMiddleware.apiCallMiddleware(), persistor.createMiddleware()],
  );

  MyApp() {
    persistor.start(store);
  }

  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Flutter Boilerplate',
      theme: new ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: new MyHomePage(title: 'Flutter Demo Home Page', store: store),
    );
  }
}