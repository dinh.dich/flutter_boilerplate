import 'package:flutter/material.dart';
import '../models/post/post.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';
import '../redux/app_state_reducer.dart';
import '../actions/post_actions.dart';

class MyHomePage extends StatelessWidget {
  final Store<AppState> store;
  MyHomePage({Key key, this.store, this.title}) : super(key: key);

  final String title;

  @override
  Widget build(BuildContext context) {
    return new StoreProvider(
        store: store,
        child: new Scaffold(
          appBar: new AppBar(
            title: new Text(title),
          ),
          body: new Center(
            child: new Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                new Text(
                  'Post title:',
                ),
                new StoreConnector<AppState, Post>(
                    converter: (store) => store.state.post,
                    builder: (context, post) {
                      return new Text(
                        '${post != null ? post.title : 'title'}',
                        style: Theme
                            .of(context)
                            .textTheme
                            .display1,
                      );
                    }),
              ],
            ),
          ),
          floatingActionButton: new FloatingActionButton(
            onPressed: () => store.dispatch(GetPost(1)),
            tooltip: 'Increment',
            child: new Icon(Icons.add),
          ), // This trailing comma makes auto-formatting nicer for build methods.
        )
    );
  }
}